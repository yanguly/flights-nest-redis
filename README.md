

# Flights Caching Api

This project was generated using [Nx](https://nx.dev).

## Toolset
  -  Nestjs
  -  Nx
  -  Redis 6.2
  -  Jest
  -  Docker and Docker-compose

## Structure
Application uses basic NX and Nest structure:
  - Flights app
  - App module – parent
  - Flights module – child

## Scalability
  App can be simply split to 2 or more running pods.
  Redis can be scaled horizontally, or Redis enterprise can be used. 

  <p style="text-align: center;"><img src="https://www.allthingsdistributed.com/images/ecredis.png" width="450"></p>

## Running app
#### Default serve
Let's start redis on port 6379
> docker run -d -p 6379:6379 --name demo-redis

Then let's start the flights app:
> npm start

The app is exposed on port 3333, you can send GET request to:
> http://localhost:3333/api/flights

#### Docker compose:
> npm run start:docker:local

App is exposed on port **3000**
GET request:
> http://localhost:3000/api/flights

## What can be done better?
- E2E API testing with Supertest or Cypress
- Kubernetes setup with load balancer and scalability
- Microservices with Redis as transport
- Search, auth
- Error handling