/* eslint-disable @typescript-eslint/no-empty-function */
import { HttpService } from '@nestjs/axios';
import { Test } from '@nestjs/testing';
import { of } from 'rxjs';
import { FlightsApiService } from './flights-api.service';
import * as mocks from '../mocks/flights.mock';

import { FlightsService } from './flights.service';

class HttpServiceMock {
  get(): any {}
}

describe('FlightsService', () => {
  let service: FlightsService;
  let apiService: FlightsApiService;

  beforeAll(async () => {
    const app = await Test.createTestingModule({
      imports: [],

      providers: [
        FlightsService,
        FlightsApiService,
        { provide: HttpService, useClass: HttpServiceMock },
      ],
    }).compile();

    service = app.get<FlightsService>(FlightsService);
    apiService = app.get<FlightsApiService>(FlightsApiService);

    service.flightsUrls = ['1', '2'];
    jest
      .spyOn(apiService, 'getFlights')
      .mockImplementation((url) =>
        url === '1' ? of(mocks.flights1) : of(mocks.flights2)
      );
  });

  describe('FlightsService', () => {
    it('should be defined', () => {
      expect(service).toBeDefined();
    });

    it('should return combined id for a flights slices', () => {
      const flight = mocks.flights1.flights[0];

      const idFromService = service.getFlightId(flight);
      const shouldBeId = '144-8542-20190808043000000-20190810053500000';

      expect(idFromService).toBe(shouldBeId);
    });

    it('should fetch api service and merge flights', (done) => {
      service.getFlights().subscribe((res) => {
        expect(res).not.toBeUndefined();
        expect(res.length).toBe(8);
        expect(res[0].id).not.toBeUndefined();
        done();
      });
    });
  });
});
