import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { map, Observable } from 'rxjs';
import { FlightsApiResponseDTO } from '../dto/flights-api.dto';

@Injectable()
export class FlightsApiService {
  constructor(private readonly httpService: HttpService) {}

  getFlights(url: string): Observable<FlightsApiResponseDTO> {
    return this.httpService.get(url).pipe(map((res) => res.data));
  }
}
