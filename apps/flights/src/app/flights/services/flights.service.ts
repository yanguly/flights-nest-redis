import { Injectable } from '@nestjs/common';
import { forkJoin, map, Observable } from 'rxjs';
import { FlightApiModel, FlightModel } from '../models/flight.model';
import { FlightsApiService } from './flights-api.service';
import { FLIGHTS_URLS } from './urls.constant';

@Injectable()
export class FlightsService {
  flightsUrls = FLIGHTS_URLS;
  constructor(private readonly apiService: FlightsApiService) {}

  getFlights(): Observable<FlightModel[]> {
    return forkJoin(
      this.flightsUrls.map((url) => this.apiService.getFlights(url))
    ).pipe(
      map((responses) =>
        this.getUniqueFlights(responses.map((r) => r.flights).flat())
      )
    );
  }

  getFlightId(flight: FlightApiModel): string {
    const [flight1, flight2] = flight.slices;

    const datesString =
      flight1.departure_date_time_utc.replace(/\D+/g, '') +
      '-' +
      flight2.departure_date_time_utc.replace(/\D+/g, '');

    return `${flight1.flight_number}-${flight2.flight_number}-${datesString}`;
  }

  getUniqueFlights(flights: FlightApiModel[]): FlightModel[] {
    const flightsWithId: Map<string, FlightApiModel> = new Map();

    flights.forEach((i) => flightsWithId.set(this.getFlightId(i), i));

    return Array.from(flightsWithId, ([id, value]) => ({
      id,
      ...value,
    }));
  }
}
