import { FlightApiModel } from "../models/flight.model";

export class FlightsApiResponseDTO {
    flights: FlightApiModel[];
}