import {
  CacheInterceptor,
  CacheKey,
  CacheTTL,
  Controller,
  Get,
  UseInterceptors,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { FlightModel } from '../models/flight.model';
import { FlightsService } from '../services/flights.service';

@UseInterceptors(CacheInterceptor)
@Controller('flights')
export class FlightsController {
  constructor(private readonly flightsService: FlightsService) {}

  @Get()
  @CacheKey('flights')
  @CacheTTL(3600)
  getFlights(): Observable<FlightModel[]> {
    return this.flightsService.getFlights();
  }
}
