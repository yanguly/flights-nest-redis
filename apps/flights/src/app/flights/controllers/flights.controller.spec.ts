/* eslint-disable @typescript-eslint/no-empty-function */
import { HttpService } from '@nestjs/axios';
import { CACHE_MANAGER } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { FlightsApiService } from '../services/flights-api.service';
import { FlightsService } from '../services/flights.service';
import { FlightsController } from './flights.controller';
import * as mocks from '../mocks/flights.mock';
import { of } from 'rxjs';

const mockCacheManager = {
  set: jest.fn(),
  get: jest.fn(),
  del: jest.fn(),
  reset: jest.fn(),
};

class HttpServiceMock {
  get(): any {}
}

describe('AppController', () => {
  let app: TestingModule;
  let service: FlightsService;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      controllers: [FlightsController],
      providers: [
        FlightsService,
        FlightsApiService,
        { provide: CACHE_MANAGER, useValue: mockCacheManager },
        { provide: HttpService, useClass: HttpServiceMock },
      ],
    }).compile();

    service = app.get<FlightsService>(FlightsService);

    service.flightsUrls = ['1', '2'];
    jest
      .spyOn(service, 'getFlights')
      .mockReturnValue(of(mocks.flights1.flights));
  });

  describe('getFlights', () => {
    it('should init', () => {
      const appController = app.get<FlightsController>(FlightsController);
      expect(appController).not.toBeUndefined();
    });

    it('should pass data', (done) => {
      const appController = app.get<FlightsController>(FlightsController);

      appController.getFlights().subscribe((i) => {
        expect(i).not.toBeUndefined();
        done();
      });
    });
  });
});
