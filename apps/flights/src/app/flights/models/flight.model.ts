export class FlightModel {
    slices: SlicesModel[];
    id: string;
    price: number;
}

export class SlicesModel {
    origin_name: string;
    destination_name: string;
    departure_date_time_utc: string;
    arrival_date_time_utc: string;
    flight_number: string;
    duration: number
}

export type FlightApiModel = Omit<FlightModel, 'id'>