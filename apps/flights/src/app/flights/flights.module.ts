import { HttpModule } from '@nestjs/axios';
import { CacheModule, Module } from '@nestjs/common';
import { FlightsController } from './controllers/flights.controller';
import { FlightsService } from './services/flights.service';
import * as redisStore from 'cache-manager-redis-store';
import { FlightsApiService } from './services/flights-api.service';

@Module({
  imports: [
    HttpModule,
    CacheModule.register({
      store: redisStore,
      host: process.env.REDIS_HOST || 'localhost',
      port: process.env.REDIS_PORT || 6379,
    }),
  ],
  controllers: [FlightsController],
  providers: [FlightsService, FlightsApiService],
})
export class FlightsModule {}
